# Bash scripts

Bash scripts that I've written to automate things.

## Scripts:
* `sortDownloads.sh`: sorts ~/Downloads based on file extensions
  * Usage: ./sortDownloads.sh
* `webScraper.sh`: create pdf out of all of a webpage's linked pages
  * Usage: ./webScraper.sh [website] [pdf file]
* `timeToRun.sh`: Calculates the time it takes a program to run
  * Usage: ./timeToRun.sh [program] [program arguments]
* `distroSetupScript.sh`: Installs some programs && applies a few customizations to a fresh install
  * Usage: ./distroSetupScript.sh [distro]

## Completions:
* `masscan`: bash tab completion for masscan port scanner
  * Project Page: https://github.com/robertdavidgraham/masscan
* `firefox`: bash tab completion for firefox browser
* `keepassxc`: bash tab completion for keepassxc password manager
* `keepassxc-cli`: bash tab completion for keepassxc's command line interface
* `emacs`: bash tab completion for the emacs text editor
