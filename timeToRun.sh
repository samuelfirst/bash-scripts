#!/bin/bash

# Get && split starting time into array of minutes, seconds, nanoseconds
STARTING_TIME=$(date '+%M %S %N')
IFS=' '
SPLIT_STARTING_TIME=($STARTING_TIME)
unset IFS

# Run program
$@

# Get ending times && split into array
ENDING_TIME=$(date '+%M %S %N')
IFS=' '
SPLIT_ENDING_TIME=($ENDING_TIME)
unset IFS

# Subtract Starting time from ending time
# Note: This can result in negative times; we'll fix this in
#       a second.
let MINUTES="10#${SPLIT_ENDING_TIME[0]}-10#${SPLIT_STARTING_TIME[0]}"
let SECONDS="10#${SPLIT_ENDING_TIME[1]}-10#${SPLIT_STARTING_TIME[1]}"
let NANO_SECONDS="10#${SPLIT_ENDING_TIME[2]}-10#${SPLIT_STARTING_TIME[2]}"

# Fix the potential negative times && single digit times
if [ "${SECONDS%?}" == '' ]
then
    SECONDS=0$SECONDS # For some reason it won't let me add a 0 to seconds
fi

if [ "${MINUTES%?}" == '' ]
then
    MINUTES=0$MINUTES
fi


if [ ${SECONDS%${SECONDS#?}} == '-' ]
then
    let MINUTES=$MINUTES-1
    let SECONDS="60-10#${SPLIT_STARTING_TIME[1]}"
fi

if [ ${NANO_SECONDS%${NANO_SECONDS#?}} == '-' ]
then
    let SECONDS=$SECONDS-1
    let NANO_SECONDS="1000000000-10#${SPLIT_STARTING_TIME[2]}"
fi




# Print time it took to run in a nice table
echo '______________________________________'
echo '|       |Minutes|Seconds|Nano Seconds|'
echo '|------------------------------------|'
echo '|START  |     '${SPLIT_STARTING_TIME[0]}'|     '${SPLIT_STARTING_TIME[1]}'|   '${SPLIT_STARTING_TIME[2]}'|'
echo '|------------------------------------|'
echo '|END    |     '${SPLIT_ENDING_TIME[0]}'|     '${SPLIT_ENDING_TIME[1]}'|   '${SPLIT_ENDING_TIME[2]}'|'
echo '|------------------------------------|'
echo '|DIFF   |     '$MINUTES'|      '$SECONDS'|   '$NANO_SECONDS'|'
echo '--------------------------------------'
