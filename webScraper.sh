#!/bin/bash

# Scrape webpage & convert all linked pages to a pdf

curl $1 > /tmp/webScraperScrapedPage
LINK_LIST=$(grep -i -o -P '(?<=href=")[^"]*' /tmp/webScraperScrapedPage)
LINK_LIST=$LINK_LIST$(grep -i -o -P "(?<=href=')[^']*" /tmp/webScraperScrapedPage)

# Run through each link && convert it to a pdf
for link in $(echo -e $LINK_LIST)
do
    wkhtmltopdf $1/$link $link.pdf
done

# I've had some problems with wkhtmltopdf downloading multiple copies
# This should fix that
rm -r *#*

# Create list of pdfs so they can be placed in order
IFS=$'\n'
PDFLIST=($LINK_LIST)
unset IFS

COUNTER=0
for i in ${PDFLIST[@]}
do
    isInDirectory=0
    for item in *
    do
	if [ ${PDFLIST[$COUNTER]}.pdf == $item ]
	then
	   PDFLIST[$COUNTER]=${PDFLIST[$COUNTER]}.pdf
	   isInDirectory=1
	   break
	else
	    isInDirectory=0
	fi
    done

    if [ $isInDirectory == 0 ]
    then
	PDFLIST[$COUNTER]=''
    fi
	   
    COUNTER=$[COUNTER + 1]
done

# Combine pdfs into one document
pdfunite ${PDFLIST[@]} $2

# Clean up pdf files
for i in ${PDFLIST[@]}
do
    rm -rf $i
done
