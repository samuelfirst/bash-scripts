#!/bin/bash

# Set up various distros from a base install
#  install programs, set up configs, etc.
#  Usage: ./distroSetupScript.sh [distro]
#   [distro] should be lowercased

DISTRO=$1

# These are the things that are done across every distro
function distroAgnostic() {
    # Install masscan
    git clone https://github.com/robertdavidgraham/masscan
    cd masscan
    make
    sudo mv ./bin/masscan /usr/local/bin
    cd ..
    rm -r masscan

    # Extend LS_COLORS
    # Follows convention as best as possible:
    #  Blue: Directory
    #  Green: Executable or data file
    #  Sky Blue: Linked file
    #  Yellow && Black Background: Device file
    #  Pink: Graphics file
    #  Red: archive file

    # Extension of convention:
    # Orange: Audio files (dark for lossy; light for lossless)
    # Purple: Video Files (dark for lossy; light for lossless)
    # Peach: ebook files
    # Seafoam: Document files
    echo "LS_COLORS=$LS_COLORS *.7z=00;35:*.iso=38;5;9:*.vdi=38;5;9:*.vmdk=38;5;9:*.vhd=38;5;9:*.blend=38;5;5:*.obj=38;5;5:*.fbx=38;5;5:*.3ds=38;5;5:*.ply=38;5;5:*.stl=38;5;5:*.py=38;5;70:*.lisp=38;5;71:*.perl=38;5;72:*.php=38;5;76:*.rb=38;5;77:*.lua=38;5;78;*.js=38;5;79:*.r=38;5;82:*.lua=38;5;83:*.html=38;5;84:*.css=38;5;84:*.xml=38;5;84:*.xhtml=38;5;84:*.md=38;5;84:*.flac=38;5;172:*.m4a=38;5;166:*.m4b=38;5;166:*.m4p=38;5;166:*.mp3=38;5;166:*.mpc=38;5;166:*.ogg=38;5;166:*.oga=38;5;166:*.mogg=38;5;166:*.opus=38;5;166:*.raw=38;5;172:*.wav=38;5;172:*.mp4=38;5;98:*.webm=38;5;98:*.mkv=38;5;98:*.flv=38;5;98:*.f4v=38;5;98:*.ogv=38;5;98:*.avi=38;5;98:*.mov=38;5;98:*.m4v=38;5;98:*.m4a=38;5;98:*.pdf=38;5;210:*.lrf=38;5;210:*.lrx=38;5;210:*.cbr=38;5;210:*.cbz=38;5;210:*.cb7=38;5;210:*.cbt=38;5;210:*.cba=38;5;210:*.djvu=38;5;210:*.epub=38;5;210:*.pdb=38;5;210:*.fb2=38;5;210:*.xeb=38;5;210:*.ceb=38;5;210:*.ibooks=38;5;210:*.inf=38;5;21 0:*.azw=38;5;210:*.azw3=38;5;210:*.kf8=38;5;210:*.kfx=38;5;210:*.prc=38;5;210:*.mobi=38;5;210:*.pdb=38;5;210:*.odf=38;5;37:*.doc=38;5;37:*.docx=38;5;37:" >> ~/.bashrc

    # Create ~/bash && add to PATH variable
    # Also, grab the scripts from my repo && add them to ~/bash
    mkdir ~/bash
    echo "PATH=$PATH:~/bash" >> ~/.profile
    git clone https://gitlab.com/samuelfirst/bash-scripts
    mv bash-scripts/*.sh ~/bash
    chmod +x bash/*.sh

    # Set up emacs to use org-mode when editing commit messages,
    # to use the ~/.emacs-saves directory for autosaves, and
    # add melpa to package manager
    echo "(add-to-list 'auto-mode-alist '(\"COMMIT_EDITMSG\" . org-mode))" > ~/.emacs-git-commit
    git config --global core.editor 'emacs -nw -l ~/.emacs-git-commit'

    echo "(setq backup-directory-alist '((\".\" . \"~/.emacs-saves\")))" >> ~/.emacs
    echo "(add-to-list 'package-archives '(\"melpa\" \"https://melpa.org/packages\"))" >> ~/.emacs

    # Set up git
    git config --global user.name "Samuel First"
    git config --global user.email "samuelfirst@protonmail.com"

    # Set default editor as emacs
    echo 'EDITOR="emacs -nw"' >> ~/.profile
    echo 'VISUAL="emacs"' >> ~/.profile

    # Set up bash completions
    sudo cp -n ~/bash-scripts/completions/* /usr/share/bash-completion/completions
    rm -r ~/bash-scripts
}

if [ $DISTRO == 'opensuse' ]
then
    # Install programs (One at a time so if one fails, the script keeps going)
    yes | sudo zypper install git
    yes | sudo zypper install emacs
    yes | sudo zypper install htop
    yes | sudo zypper install blender
    yes | sudo zypper install gimp
    yes | sudo zypper install krita
    yes | sudo zypper install shotcut
    yes | sudo zypper install inkscape
    yes | sudo zypper install nmap
    yes | sudo zypper install clisp
    yes | sudo zypper install ffmpeg
    yes | sudo zypper install ImageMagick
    yes | sudo zypper install virtualbox
    yes | sudo zypper install transmission
    yes | sudo zypper install gcc
    yes | sudo zypper install make
    yes | sudo zypper install libpcap-devel

    # Set up packman repos && install h.264 codecs
    sudo zypper ar -f 'http://packman.jacobs-university.de/suse/openSUSE_Tumbleweed/Multimedia/' packman
    yes | sudo zypper install --from packman libavcodec56 libavcodec57 libavcodec58 libavformat56 libavformat57 libavformat58 libavdevice56 libavdevice57 libavdevice58
fi

if [ $DISTRO == 'ubuntu']
then
    yes | sudo apt-get install git
    yes | sudo apt-get install emacs
    yes | sudo apt-get install htop
    yes | sudo apt-get install blender
    yes | sudo apt-get install gimp
    yes | sudo apt-get install krita
    yes | sudo apt-get install openshot
    yes | sudo apt-get install inkscape
    yes | sudo apt-get install nmap
    yes | sudo apt-get install clisp
    yes | sudo apt-get install ffmpeg
    yes | sudo apt-get install imagemagick
    yes | sudo apt-get install virtualbox
    yes | sudo apt-get install transmission
    yes | sudo apt-get install gcc
    yes | sudo apt-get install make
    yes | sudo apt-get install libpcap-dev
fi

if [ $DISTRO == 'arch' ]
then
    yes | sudo pacman -S git
    yes | sudo pacman -S emacs
    yes | sudo pacman -S htop
    yes | sudo pacman -S blender
    yes | sudo pacman -S gimp
    yes | sudo pacman -S krita
    yes | sudo pacman -S shotcut
    yes | sudo pacman -S inkscape
    yes | sudo pacman -S nmap
    yes | sudo pacman -S clisp
    yes | sudo pacman -S ffmpeg
    yes | sudo pacman -S imagemagick
    yes | sudo pacman -S virtualbox
    yes | sudo pacman -S transmission-qt
    yes | sudo pacman -S gcc
    yes | sudo pacman -S make
    yes | sudo pacman -S libpcap
fi

if [ $DISTRO == 'debian' ]
then
    yes | sudo apt-get install git
    yes | sudo apt-get install emacs
    yes | sudo apt-get install htop
    yes | sudo apt-get install blender
    yes | sudo apt-get install gimp
    yes | sudo apt-get install krita
    yes | sudo apt-get install openshot
    yes | sudo apt-get install inkscape
    yes | sudo apt-get install nmap
    yes | sudo apt-get install gcl
    yes | sudo apt-get install ffmpeg
    yes | sudo apt-get install imagemagick
    yes | sudo apt-get install kvm
    yes | sudo apt-get install transmission
    yes | sudo apt-get install gcc
    yes | sudo apt-get install make
    yes | sudo apt-get install libpcap-dev
fi

distroAgnostic
