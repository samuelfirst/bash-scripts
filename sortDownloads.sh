#!/bin/bash

# Set path for Downloads Directory
d=~/Downloads

# Print whether sorted or not with pretty formatting
# Args: $1=0 if no files matching type found
#       $2=type of file
function printSorted {
    tput 'bold' # Make output brighter
    
    if [ $1 = 0 ]
    then
	tput 'setaf' 3  # Set foreground color to yellow
	echo 'No '$2' found'
    else
	tput 'setaf' 2  # Set foreground color to green
	echo $2' sorted'
    fi

    tput 'sgr0' # Reset colors/brightness to default
}

# For each file extension:
#  If there's any files of that type:
#   Move those files to their assosciated directory
#   Return 0
#  Else:
#   Return 1
function moveFiles {
    exists=1
    
    for extension in $@
    do
	count=$(ls $d/$extension 2> /dev/null | wc -l) || count=0
	if [ $count -gt 0 ]
	then
	    mv -i $d/$extension ${@: -1}
	    exists=0
	fi
    done

    return $exists
}

# If passed -q flag, 

# Move e-books to books directory
echo -n 'Sorting e-books...'
noBooks=1
moveFiles *.pdf *.epub *.pdb ~/Documents/Books || noBooks=0
printSorted $noBooks 'e-books'

# Move music to music directory
echo -n 'Sorting music...'
noMusic=1
moveFiles *.mp3 *.wav *.ogg ~/Music || noMusic=0
printSorted $noMusic 'music'

# Move videos to videos directory
echo -n 'Sorting Videos...'
noVideos=1
moveFiles *.mp4 *.webm *.ogv *.mpv *.mkv *.avi *.mov ~/Videos || noVideos=0
printSorted $noVideos 'videos'

# Move pictures to pictures directory
echo -n 'Sorting Pictures...'
noPictures=1
moveFiles *.jpg *.jpeg *.svg *.png *.gif *.bmp ~/Pictures || noPictures=0
printSorted $noPictures 'pictures'
